<h1>Nyhedsarkiv</h1>
<h2>Public Relations, begivenhder mv.</h2>

<div class="container">
    <div class="row">
        <div class="blog-wrap">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php
                    $args = array( 'posts_per_page' => 10, 'order'=> 'DECS', 'orderby' => 'date' );
                    $postslist = get_posts( $args );
                    foreach ( $postslist as $post ) :
                      setup_postdata( $post ); ?>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                            <?php echo get_the_post_thumbnail( $post_id, 'full', array( 'class' => 'img-responsive' ) ); ?>
                            </div>
                            <div class="col-md-8 col-xs-12"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title( '<h2>', '</h2>' ); ?></a>
                                <div class="excerpt"><?php the_excerpt(); ?></div>
                                <a class="btn btn-default btn-rolink" href="<?php the_permalink(); ?>">Læs mere <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <hr><br>
                    <?php
                endforeach;
                wp_reset_postdata();
                ?>
                <?php wp_link_pages( $args ); ?>
                <?php endwhile; else: ?>
                <?php endif; ?>
        </div>
    </div>
</div>
