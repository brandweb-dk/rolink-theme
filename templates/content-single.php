<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>
<hr>
<p><span class="dato"><i class="fa fa-clock-o"></i> Indlæg fra d. <?php the_date(); ?></span><span class="tags"> <?php the_tags( 'Tags: ', ', ', '<br />' ); ?> </span></p>
<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
