<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <?php dynamic_sidebar('footer-1'); ?>
      </div>

      <div class="col-md-4 footer-mobile-spacing">
        <?php dynamic_sidebar('footer-2'); ?>
      </div>

      <div class="col-md-4 social">
        <?php dynamic_sidebar('footer-3'); ?>
      </div>
    </div>
  </div>
</footer>
